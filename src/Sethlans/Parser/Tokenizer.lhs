> module Sethlans.Parser.Tokenizer
>   (tokenizer)
> where
>
> import Control.Monad
> import Data.Char
> import Data.Maybe
> import Numeric
> import Text.Parsec hiding (token)
> import Text.Parsec.Text
> import Sethlans.Parser.Token
>   (Token(..), TokenType(..), TokenValue(..))

Sethlans tokenizer, transforms a character stream into a token stream
for parsing.

> tokenizer :: Parser [Token]
> tokenizer = do
>   a <- many (try (line <* newline))
>   b <- line <* eof
>   pure $ join a ++ b

A source code line starts with ">" and contains any number of
tokens. Lines not starting with ">" are ignored.

> line :: Parser [Token]
> line = (char '>' *> many (sp *> token) <* sp) <|>
>        ([] <$ manyTill anyToken (lookAhead (void newline <|> eof)))
>
> sp :: Parser ()
> sp = skipMany (satisfy $ \c -> isSpace c && c /= '\n')

A token is a special character (parenthese, quote), a string, an int,
or a symbol.

> token :: Parser Token
> token = do
>   pos <- getPosition
>   (typ, val) <- (,TvNone) <$> special <|> str <|> try int <|> symbol
>   pure $ Token pos typ val
>
> special :: Parser TokenType
> special =
>   TokLeftParen <$ char '(' <|>
>   TokRightParen <$ char ')' <|>
>   TokQuote <$ char '\''

A string is a character string between double quotes.

> str :: Parser (TokenType, TokenValue)
> str = do
>   _ <- char '"'
>   s <- manyTill (escape <|> anyToken) (try $ char '"')
>   pure (TokString, TvString s)

An escape sequence within a string starts with a backslash, and has
multiple possible forms:

- \\, \" - Literal \ and " respectively.
- \xNN, \uNNNN, \wNNNNNN - 8, 16 and 24-bit Unicode codepoints in hex.
- \n, \r, \t, \v, \f - The usual C meanings.

> escape :: Parser Char
> escape = do
>   _ <- char '\\'
>   c <- anyToken
>   let ch n = chr . fst . head . readHex <$>
>              count n hexDigit
>   let m = [('\\', pure '\\')
>           ,('"', pure '"')
>           ,('n', pure '\n')
>           ,('r', pure '\r')
>           ,('t', pure '\t')
>           ,('v', pure '\v')
>           ,('f', pure '\f')
>           ,('x', ch 2)
>           ,('u', ch 4)
>           ,('w', ch 6)]
>   fromMaybe (fail $ "invalid escape sequence " ++ show c)
>     (lookup c m)

An int is a base 10 decimal number, optionally preceeded by a sign
character.

> int :: Parser (TokenType, TokenValue)
> int = do
>   sign <- try ((-1) <$ char '-' <|> 1 <$ char '+') <|> pure 1
>   num <- read <$> many1 digit
>   pure (TokInt, TvInt (sign * num))

A symbol is everything else - a string of characters ending at a
special character (except ', which may be part of a symbol) or space.

> symbol :: Parser (TokenType, TokenValue)
> symbol = do
>   let f c = not (isSpace c || c `elem` ("()" :: String))
>   sym <- many1 (satisfy f)
>   pure (TokSymbol, TvString sym)
