> module Sethlans.Parser.Token
>   (Token(..), TokenType(..), TokenValue(..)
>   ,Parser, satisfy, exactly)
> where
>
> import Text.Parsec.Prim
> import Text.Parsec.Pos

Parsed token of Sethlans source code.

> data TokenType
>   = TokLeftParen
>   | TokRightParen
>   | TokQuote
>   | TokString
>   | TokInt
>   | TokSymbol
>   deriving (Eq, Enum)
>
> instance Show TokenType where
>   show TokLeftParen = "\"(\""
>   show TokRightParen = "\")\""
>   show TokQuote = "\"'\""
>   show TokString = "string"
>   show TokInt = "int"
>   show TokSymbol = "symbol"
>
> data TokenValue
>   = TvNone
>   | TvInt { tvInt :: Integer }
>   | TvString { tvString :: String }
>
> data Token = Token
>   { tokenPos :: SourcePos
>   , tokenType :: TokenType
>   , tokenValue :: TokenValue }
>
> instance Show Token where
>   show = show . tokenType

Parsec-related definitions for parsing a token stream.

> type Parser a = Parsec [Token] () a
>
> satisfy :: (Token -> Bool) -> Parser Token
> satisfy p = token (show . tokenType) tokenPos $ \t ->
>   if p t then Just t else Nothing
>
> exactly :: TokenType -> Parser Token
> exactly e = satisfy $ \t -> tokenType t == e
