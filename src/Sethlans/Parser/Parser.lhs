> module Sethlans.Parser.Parser
>   (parser)
> where
>
> import Control.Comonad.Cofree
> import Sethlans.Parser.Ast
> import Sethlans.Parser.Token
> import Text.Parsec

Top-level parser, parse all possible expressions.

> parser :: Parser Ast
> parser = do
>   pos <- getPosition
>   ast <- AstList <$> many expr
>   _ <- eof
>   pure $ pos :< ast

Expression - a list, quote, int or symbol.

> expr :: Parser Ast
> expr = do
>   pos <- getPosition
>   ast <- list <|> quote <|> str <|> int <|> symbol
>   pure $ pos :< ast

A list is made of any number of expressions between parentheses.

> list :: Parser (Ast' Ast)
> list = AstList <$>
>   between
>   (exactly TokLeftParen)
>   (exactly TokRightParen)
>   (many expr)

A quote is made of a quote followed by an expression. Translation to
(quote) is not done at the parser level.

> quote :: Parser (Ast' Ast)
> quote = AstQuote <$> (exactly TokQuote *> expr)

Strings, ints, and symbols are just that, ints and symbols.

> str :: Parser (Ast' Ast)
> str = AstString . tvString . tokenValue <$> exactly TokString
>
> int :: Parser (Ast' Ast)
> int = AstInt . tvInt . tokenValue <$> exactly TokInt
>
> symbol :: Parser (Ast' Ast)
> symbol = AstSymbol . tvString . tokenValue <$> exactly TokSymbol
