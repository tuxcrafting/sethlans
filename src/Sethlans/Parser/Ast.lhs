> module Sethlans.Parser.Ast
>   (Ast, Ast'(..))
> where
>
> import Control.Comonad.Cofree
> import Data.Functor.Classes
> import Text.Parsec.Pos

Abstract syntax tree. An AST is represented as a cofree comonad of
elements annotated with their position.

> data Ast' a
>   = AstList [a]
>   | AstQuote a
>   | AstString String
>   | AstInt Integer
>   | AstSymbol String
>   deriving Functor
>
> instance Show1 Ast' where
>   liftShowsPrec f g p x = case x of
>     AstList as -> ("(AstList " ++) . g as . (")" ++)
>     AstQuote a -> ("(AstQuote " ++) . f p a . (")" ++)
>     AstString s -> ("(AstString " ++) . showsPrec p s . (")" ++)
>     AstInt n -> ("(AstInt " ++) . showsPrec p n . (")" ++)
>     AstSymbol s -> ("(AstSymbol " ++) . (s ++) . (")" ++)
>
> type Ast = Cofree Ast' SourcePos
