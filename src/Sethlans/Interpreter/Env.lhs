> module Sethlans.Interpreter.Env
>   (initEnv)
> where
>
> import Control.Lens hiding (Lazy)
> import Control.Monad.Except
> import Data.Array.IArray
> import Data.Char
> import qualified Data.Text as T
> import Sethlans.Data
> import Sethlans.Data.Other
> import Sethlans.Interpreter.Builtin
> import Sethlans.Interpreter.Compiler
> import Sethlans.Interpreter.Eval
> import Sethlans.Interpreter.Types
> import Sethlans.Parser
> import System.Console.Haskeline

Initialize the environment with functions necessary to bootstrap the
library.

> initEnv :: Interpreter ()
> initEnv = do
>
>   -- Compile and evaluate some code (local bindings are not passed,
>   -- use macros for that!).
>   makeBuiltin 1 "eval" $ \[x] -> do
>     code <- compile x
>     eval code
>
>   -- Parse S-exprs.
>   makeBuiltin 1 "parse" $ \[s] -> do
>     s' <- seth2hask s
>     case parseSethlans "(parse)" (T.pack s') of
>       Left e -> throwError $ show e
>       Right x -> quote x
>
>   -- Basic integer arithmetic.
>   let intOp2 name f = makeBuiltin 2 name $ \[x, y] -> do
>         x' <- getLazySafe intObj' x
>         y' <- getLazySafe intObj' y
>         makeLazy' $ intObj $ f x' y'
>
>   intOp2 "-" $ \x y -> x - y
>   intOp2 "*" $ \x y -> x * y
>   intOp2 "/" $ \x y -> x `div` y
>   intOp2 "%" $ \x y -> x `rem` y
>
>   -- 3-way integer comparison.
>   makeBuiltin 5 "cmp" $ \[x, y, lt, eq, gt] -> do
>     x' <- getLazySafe intObj' x
>     y' <- getLazySafe intObj' y
>     pure $ case x' `compare` y' of
>       LT -> lt
>       EQ -> eq
>       GT -> gt
>
>   trueObj <- symbolObj <$> stSymbolFromName "t"
>
>   -- Symbol equality.
>   makeBuiltin 2 "sym-eq-p" $ \[x, y] -> do
>     x' <- getLazySafe symbolObj' x
>     y' <- getLazySafe symbolObj' y
>     makeLazy' $ if x' == y' then trueObj else nullObj
>
>   -- Boolean choice, three arguments, second is chosen if
>   -- first is null else third.
>   makeBuiltin 3 "?" $ \[k, x, y] -> do
>     k' <- nullObj' <$> getLazy k
>     pure $ if k' then x else y
>
>   -- List construction.
>   makeBuiltin 2 "cons" $ \[x, xs] -> makeLazy' $ consObj x xs
>
>   -- Get type of an object.
>   makeBuiltin 1 "type-of" $ \[x] -> do
>     x' <- getLazy x
>     makeLazy' $ symbolObj $ x' ^. typeSym
>
>   -- Get a slot.
>   makeBuiltin 2 "slot" $ \[sl, x] -> do
>     sl' <- getLazySafe intObj' sl
>     x' <- getLazy x
>     let y = x' ^? slot (fromInteger sl')
>     maybe (throwError "out of bounds") pure y
>
>   -- Get the number of slots.
>   makeBuiltin 1 "slots" $ \[x] -> do
>     x' <- getLazy x
>     makeLazy' $ intObj $ x' ^. slots . to bounds . _2 . to toInteger
>
>   -- Build an object.
>   makeBuiltin 2 "object" $ \[typ, sls] -> do
>     typ' <- getLazySafe symbolObj' typ
>     sls' <- orInvalid $ foldrObj (\x z -> pure (x:z)) [] getLazy sls
>     makeLazy' $ Object typ'
>       (listArray (1, length sls') sls') MagicNone
>
>   -- Convert between symbols and their names.
>   makeBuiltin 1 "symbol-from-name" $ \[name] -> do
>     name' <- seth2hask name
>     sym <- stSymbolFromName name'
>     makeLazy' $ symbolObj sym
>   makeBuiltin 1 "symbol-to-name" $ \[sym] -> do
>     Symbol sym' <- getLazySafe symbolObj' sym
>     name <- orInvalid $ use $ stSymbols . at sym'
>     hask2seth name
>
>   -- Set and get globals and macros.
>   let setIn suffix f g = do
>         makeBuiltin 2 ("set-" ++ suffix ++ "") $ \[sym, val] -> do
>           sym' <- getLazySafe symbolObj' sym
>           _ <- f sym' $ Just val
>           makeLazy' nullObj
>         makeBuiltin 1 ("unset-" ++ suffix ++ "") $ \[sym] -> do
>           sym' <- getLazySafe symbolObj' sym
>           _ <- f sym' Nothing
>           makeLazy' nullObj
>         makeBuiltin 1 ("get-" ++ suffix ++ "#") $ \[sym] -> do
>           sym' <- getLazySafe symbolObj' sym
>           g sym' >>= \case
>             Nothing -> throwError "unbound global"
>             Just x -> pure x
>
>   setIn "global" (\s v -> stGlobal s .= v) (\s -> use $ stGlobal s)
>   setIn "macro" (\s v -> stMacro s .= v) (\s -> use $ stMacro s)
>
>   -- Evaluate first argument to head normal form, return second
>   -- argument.
>   makeBuiltin 2 "seq" $ \[x, y] -> do
>     _ <- getLazy x
>     pure y
>
>   -- Evaluate an expression, calling a handler with the error if it
>   -- throws.
>   makeBuiltin 2 "catch#" $ \[x, y] ->
>     catchError (x <$ getLazy x) $ \e -> do
>     e' <- hask2seth e
>     eval (Apply (Literal y) (Literal e'))
>
>   -- Impure IO stuff.
>   makeBuiltin 1 "error#" $ \[_] -> do
>     throwError "error"
>
>   makeBuiltin 1 "write-char#" $ \[c] -> do
>     c' <- getLazySafe intObj' c
>     liftIO $ putChar $ chr $ fromInteger c'
>     pure c
>
>   makeBuiltin 1 "read-line#" $ \[_] -> do
>     s <- liftIO $
>       runInputT defaultSettings $
>       getInputLine ""
>     maybe (throwError "eof") hask2seth s
>
>   makeBuiltin 1 "trace#" $ \[c] -> do
>     liftIO . print =<< getLazy c
>     pure c

Try deconstructing a lazy value, erroring out if not possible.

> getLazySafe :: (Object -> Maybe a) -> Lazy -> Interpreter a
> getLazySafe f x = orInvalid $ f <$> getLazy x
>
> orInvalid :: Interpreter (Maybe a) -> Interpreter a
> orInvalid = (>>= maybe (throwError "invalid type") pure)

Convert between Sethlans strings and Haskell strings.

> seth2hask :: Lazy -> Interpreter String
> seth2hask = getLazy >=> \t -> case nullObj' t of
>   True -> pure []
>   False -> do
>     (x, xs) <- orInvalid $ pure (consObj' t)
>     n <- getLazySafe intObj' x
>     let c = chr $ fromInteger n
>     cs <- seth2hask xs
>     pure (c:cs)
>
> hask2seth :: String -> Interpreter Lazy
> hask2seth [] = makeLazy' nullObj
> hask2seth (c:cs) = do
>   x <- makeLazy' $ intObj $ toInteger $ ord c
>   xs <- hask2seth cs
>   makeLazy' $ consObj x xs
