> module Sethlans.Interpreter.Types
>   (Interpreter, BuiltinFn(..)
>   ,IntState(..), emptyState, stSymbols
>   ,stGlobals, stGlobal
>   ,stMacros, stMacro
>   ,stBuiltins, stBuiltin
>   ,stSymbolFromName)
> where
>
> import Control.Lens hiding (Lazy)
> import Control.Monad.Except
> import Control.Monad.State
> import qualified Data.IntMap.Strict as M
> import Sethlans.Data

The monad on which the Sethlans interpreter runs.

> type Interpreter a = ExceptT String (StateT IntState IO) a

A builtin function is indexed by an Int, takes in as argument a list
of lazy objects and returns a lazy object.

> newtype BuiltinFn = BuiltinFn
>   { runBuiltin :: [Lazy] -> Interpreter Lazy }

The state of the interpreter: symbols, globals, builtins, macros.

> data IntState = IntState
>   { _stSymbols :: SymbolMap
>   , _stGlobals :: M.IntMap Lazy
>   , _stMacros :: M.IntMap Lazy
>   , _stBuiltins :: M.IntMap BuiltinFn }
>
> emptyState :: IntState
> emptyState = IntState defaultSymbols M.empty M.empty M.empty

Lenses.

> makeLenses ''IntState
> stGlobal :: Symbol -> Lens' IntState (Maybe Lazy)
> stGlobal = (stGlobals .) . at . symbolNum
> stMacro :: Symbol -> Lens' IntState (Maybe Lazy)
> stMacro = (stMacros .) . at . symbolNum
> stBuiltin :: Int -> Lens' IntState (Maybe BuiltinFn)
> stBuiltin = (stBuiltins .) . at

Helper function to operate on the symbol map of the current state.

> stSymbolFromName :: String -> Interpreter Symbol
> stSymbolFromName name = do
>   sm <- use stSymbols
>   let (sym, sm') = symbolFromName sm name
>   stSymbols .= sm'
>   pure sym
