> module Sethlans.Interpreter.Builtin
>   (makeBuiltin)
> where
>
> import Control.Lens hiding (Lazy)
> import Data.Function.HT
> import qualified Data.IntMap.Strict as M
> import Sethlans.Data
> import Sethlans.Interpreter.Types

Create a builtin with a certain number of arguments.

> makeBuiltin :: Int -> String
>             -> ([Lazy] -> Interpreter Lazy)
>             -> Interpreter ()
> makeBuiltin args name f = do
>   sym <- stSymbolFromName name
>   n <- M.size <$> use stBuiltins
>   let k = nest (args - 1) Abstract $
>           Builtin n $ reverse $ take args $
>           iterate (\(Local i) -> Local (i + 1)) (Local 1)
>   k' <- makeLazy' $ codeObj k
>   stGlobal sym .= Just k'
>   stBuiltin n .= Just (BuiltinFn f)
