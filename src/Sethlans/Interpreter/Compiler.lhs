> module Sethlans.Interpreter.Compiler
>   (quote, compile)
> where
>
> import Control.Applicative
> import Control.Comonad.Cofree
> import Control.Lens hiding (Lazy, (:<))
> import Control.Monad.Except
> import Control.Monad.Reader
> import Control.Monad.State
> import Data.Char
> import Data.Foldable
> import Data.Maybe
> import qualified Data.IntMap.Strict as M
> import Sethlans.Data
> import Sethlans.Interpreter.Eval
> import Sethlans.Interpreter.Types
> import Sethlans.Parser.Ast

Convert an AST into an object list representation.

> quote :: Ast -> Interpreter Lazy
> quote = quote' . unwrap
>
> quote' :: Ast' Ast -> Interpreter Lazy
> quote' (AstList xs) = do
>   t <- makeLazy' nullObj
>   foldrM (\x z -> do { x' <- quote x; makeLazy' (consObj x' z) }) t xs
> quote' (AstQuote x) = quote' $
>   AstList [undefined :< (AstSymbol "quote"), x]
> quote' (AstString s) = quote' $
>   AstQuote ((undefined :<) $ AstList $
>             (undefined :<) . AstInt . toInteger . ord <$> s)
> quote' (AstInt n) = makeLazy' $ intObj n
> quote' (AstSymbol name) = do
>   sym <- stSymbolFromName name
>   makeLazy' $ symbolObj sym

Produce code from an object.

> compile :: Lazy -> Interpreter Code
> compile x = runReaderT (compile' x) (0, M.empty)

This type synonym HAS to be fully expanded because GHC won't let me
use InterpreterT partially applied (even with LiberalTypeSynonyms on,
although to be entirely honest I am not completely sure what that
extension even /does/).

> type Compilation a = ReaderT (Int, M.IntMap Int) (ExceptT String (StateT IntState IO)) a
>
> getLazy' :: Lazy -> Compilation Object
> getLazy' = lift . getLazy
>
> compile' :: Lazy -> Compilation Code
> compile' code = do
>   code' <- getLazy' code
>   fromMaybe (pure $ Literal code) $
>     compileApplyMacro <$> consObj' code' <|>
>     compileSymbol <$> symbolObj' code'

Check if the function application is a macro (incl a builtin one), in
which case expand it, else compile it as a simple application.

> compileApplyMacro :: (Lazy, Lazy) -> Compilation Code
> compileApplyMacro (x, xs) = symbolObj' <$> lift (getLazy x) >>= \case
>   Just sym -> case sym of
>     Symbol 5 {- quote -} -> compileQuote xs
>     Symbol 6 {- \ -} -> compileLambda xs
>     _ -> use (stMacro sym) >>= \case
>       Just m -> lift (eval (Apply (Literal m) (Literal xs))) >>= compile'
>       Nothing -> compileApply x xs
>   Nothing -> compileApply x xs
>
> compileApply :: Lazy -> Lazy -> Compilation Code
> compileApply x xs = do
>   x' <- compile' x
>   compileApply' x' xs
>
> compileApply' :: Code -> Lazy -> Compilation Code
> compileApply' x xs = consObj' <$> getLazy' xs >>= \case
>   Just (a, as) -> do
>     a' <- compile' a
>     compileApply' (Apply x a') as
>   Nothing -> pure x

(quote x) and (\ args body) are builtin, special-case macros, as they
are pretty much necessary for bootstrapping.

> compileQuote :: Lazy -> Compilation Code
> compileQuote xs = do
>   (a, t) <- tryFromJust =<< consObj' <$> lift (getLazy xs)
>   ensureNull =<< lift (getLazy t)
>   pure $ Literal a
>
> compileLambda :: Lazy -> Compilation Code
> compileLambda xs = do
>   (a, t) <- tryFromJust =<< consObj' <$> lift (getLazy xs)
>   (b, u) <- tryFromJust =<< consObj' <$> lift (getLazy t)
>   ensureNull =<< lift (getLazy u)
>
>   let f x = consObj' <$> lift (getLazy x) >>= \case
>         Nothing -> compile' b
>         Just (arg, args) -> do
>           Symbol arg' <- tryFromJust =<< symbolObj' <$> lift (getLazy arg)
>           Abstract <$>
>             local (\(i, m) -> (i + 1, m & at arg' .~ Just i))
>             (f args)
>   f a
>
> tryFromJust :: Maybe a -> Compilation a
> tryFromJust = maybe (throwError "invalid type") pure
>
> ensureNull :: Object -> Compilation ()
> ensureNull obj = unless (nullObj' obj) $ throwError "invalid type"

Compile in a symbol, as a local reference if it is in the local scope,
else as a global reference.

> compileSymbol :: Symbol -> Compilation Code
> compileSymbol sym'@(Symbol sym) = view (_2 . at sym) >>= \case
>   Just n -> do
>     m <- view _1
>     let i = m - n
>     pure $ Local i
>   Nothing -> pure $ Global sym'
