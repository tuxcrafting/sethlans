> module Sethlans.Interpreter.Eval
>   (getLazy, eval)
> where
>
> import Control.Lens hiding (Lazy)
> import Control.Monad.Except
> import Data.IORef
> import Sethlans.Data
> import Sethlans.Interpreter.Types

Get the value stored in a lazy object, evaluating it if needed.

> getLazy :: Lazy -> Interpreter Object
> getLazy (Lazy ref) = liftIO (readIORef ref) >>= \case
>   Left code -> do
>     obj <- eval code >>= getLazy
>     liftIO $ ref `writeIORef` (Right obj)
>     pure obj
>   Right obj -> pure obj

Evaluate some code. Function applications do not recursively evaluate
but instead build up lazy values.

> eval :: Code -> Interpreter Lazy
> eval (Literal x) = pure x
> eval (Local _) = throwError "free local in evaluation"
> eval (Global sym) = do
>   x <- use $ stGlobal sym
>   maybe unbound pure x where
>     unbound = do
>       sm <- use $ stSymbols
>       throwError $ "unbound global " ++
>         maybe "<unk>" show (symbolToName sm sym)
> eval (Abstract code) = makeLazy' (codeObj code)
> eval (Apply f x) = do
>   f' <- getLazy =<< eval f
>   k <- maybe (throwError "not a code object") pure (codeObj' f')
>   x' <- makeLazy x
>   makeLazy $ apply k x'
> eval (Builtin n xs) = do
>   f <- use $ stBuiltin n
>   let xs' = [x | Literal x <- xs]
>   maybe (throwError "invalid builtin") (($ xs') . runBuiltin) f
