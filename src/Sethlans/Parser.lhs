> module Sethlans.Parser
>   (Ast, Ast'(..)
>   ,parseSethlans)
> where
>
> import Sethlans.Parser.Ast
> import Sethlans.Parser.Parser
> import Sethlans.Parser.Tokenizer
> import qualified Data.Text as T
> import Text.Parsec hiding (tokens)

Full parser, tokenize and parse an input text.

> parseSethlans :: SourceName -> T.Text -> Either ParseError Ast
> parseSethlans name text = do
>   tokens <- runParser tokenizer () name text
>   runParser parser () name tokens
