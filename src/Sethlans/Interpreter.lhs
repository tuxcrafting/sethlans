> module Sethlans.Interpreter
>   (Interpreter
>   ,BuiltinFn(..), makeBuiltin, getLazy
>   ,eval, evalAst
>   ,initEnv
>   ,runInterpreter
>   ,Ast, Ast'(..), parseSethlans
>   ,Lazy, Object(..), Symbol(..))
> where
>
> import Control.Monad
> import Control.Monad.Except
> import Control.Monad.State
> import Sethlans.Data
> import Sethlans.Interpreter.Builtin
> import Sethlans.Interpreter.Compiler
> import Sethlans.Interpreter.Env
> import Sethlans.Interpreter.Eval
> import Sethlans.Interpreter.Types
> import Sethlans.Parser

> runInterpreter :: Interpreter a
>                -> IO (Either String a)
> runInterpreter it = fst <$> runStateT (runExceptT it) emptyState
>
> evalAst :: Ast -> Interpreter Lazy
> evalAst = quote >=> compile >=> eval
