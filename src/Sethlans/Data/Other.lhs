> module Sethlans.Data.Other
>   (foldrObj)
> where
>
> import Sethlans.Data

Various useful functions for operating over objects.

Attempt to right-fold over cons lists, with a provided lazy evaluation
function. Note that this is strict.

> foldrObj :: Monad m
>          => (Lazy -> a -> m a) -> a
>          -> (Lazy -> m Object) -> Lazy
>          -> m (Maybe a)
> foldrObj f z g x = do
>   x' <- g x
>   if nullObj' x'
>     then pure (Just z)
>     else case consObj' x' of
>     Just (a, b) -> traverse (f a) =<< (foldrObj f z g b)
>     Nothing -> pure Nothing

>
