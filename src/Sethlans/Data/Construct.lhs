> module Sethlans.Data.Construct
>   (make
>   ,nullObj, consObj, intObj, symbolObj, codeObj
>   ,nullObj', consObj', intObj', symbolObj', codeObj')
> where
>
> import Control.Lens
> import Data.Array.IArray
> import Sethlans.Data.Symbols
> import Sethlans.Data.Types

Constructor for basic types.

> make :: String -> [Lazy] -> MagicSlot -> Object
> make name sl mag = Object (defSym name) (listArray (1, length sl) sl) mag
>
> nullObj :: Object
> nullObj = make "null" [] MagicNone
>
> consObj :: Lazy -> Lazy -> Object
> consObj x xs = make "cons" [x, xs] MagicNone
>
> intObj :: Integer -> Object
> intObj n = make "int" [] (MagicInt n)
>
> symbolObj :: Symbol -> Object
> symbolObj sym = make "symbol" [] (MagicSymbol sym)
>
> codeObj :: Code -> Object
> codeObj code = make "code" [] (MagicCode code)

As well as the matching deconstructors.

> checkType :: String -> Object -> Maybe ()
> checkType name obj
>   | obj ^. typeSym == defSym name = Just ()
>   | otherwise = Nothing
>
> nullObj' :: Object -> Bool
> nullObj' obj = obj ^. typeSym == defSym "null"
>
> consObj' :: Object -> Maybe (Lazy, Lazy)
> consObj' obj = do
>   checkType "cons" obj
>   x <- obj ^? slot 1
>   xs <- obj ^? slot 2
>   pure (x, xs)
>
> intObj' :: Object -> Maybe Integer
> intObj' obj = do
>   checkType "int" obj
>   obj ^? magicInt
>
> symbolObj' :: Object -> Maybe Symbol
> symbolObj' obj = do
>   checkType "symbol" obj
>   obj ^? magicSymbol
>
> codeObj' :: Object -> Maybe Code
> codeObj' obj = do
>   checkType "code" obj
>   obj ^? magicCode
