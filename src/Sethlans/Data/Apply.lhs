> module Sethlans.Data.Apply
>   (apply)
> where
>
> import Control.Monad.Reader
> import Data.Generics.Uniplate.Direct
> import Sethlans.Data.Types

Apply an argument to a lambda abstraction, substituting all relevant
local references with the argument as a literal.

> apply :: Code -> Lazy -> Code
> apply code obj = runReader (go code) 1 where
>   go x@(Local n) = do
>     l <- ask
>     pure $ if l == n then (Literal obj) else x
>   go x@(Abstract _) = local succ (descendM go x)
>   go x = descendM go x
