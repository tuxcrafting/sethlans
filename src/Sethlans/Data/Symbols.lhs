> module Sethlans.Data.Symbols
>   (SymbolMap, symbolFromName, symbolToName
>   ,defaultSymbols, defSym)
> where
>
> import Control.Applicative
> import qualified Data.IntMap.Strict as M
> import Sethlans.Data.Types

Map from symbols to their name and associated helper functions.

> type SymbolMap = M.IntMap String
>
> symbolFromName :: SymbolMap -> String -> (Symbol, SymbolMap)
> symbolFromName sm name =
>   maybe create (,sm) $
>   M.foldrWithKey (\k v z -> f k v <|> z) Nothing sm where
>   f sym name'
>     | name == name' = Just (Symbol sym)
>     | otherwise = Nothing
>   create = let sym = M.size sm
>     in (Symbol sym, M.insert sym name sm)
>
> symbolToName :: SymbolMap -> Symbol -> Maybe String
> symbolToName sm (Symbol sym) = M.lookup sym sm

Default symbol map.

> defaultSymbols :: SymbolMap
> defaultSymbols = M.fromAscList $ zip [0..]
>   ["null", "int", "symbol", "code", "cons", "quote", "\\"]
>
> defSym :: String -> Symbol
> defSym = fst . symbolFromName defaultSymbols
