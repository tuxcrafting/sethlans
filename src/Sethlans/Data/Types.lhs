> module Sethlans.Data.Types
>   (Symbol(..)
>   ,Lazy(..), Object(..), typeSym, slots, slot, magic
>   ,MagicSlot(..), magicInt, magicSymbol, magicCode
>   ,Code(..), makeLazy, makeLazy')
> where
>
> import Control.Lens hiding (Lazy, plate)
> import Control.Monad.IO.Class
> import Data.Array.IArray
> import Data.Generics.Uniplate.Direct
> import Data.IORef

A symbol is an unique value associated with a string, used to uniquely
identify names.

> newtype Symbol = Symbol
>   { symbolNum :: Int }
>   deriving (Eq, Ord, Show)

A Sethlans object is a fixed-length container of slots. Slots are
numbered from 1, and may contain any value. There may optionally be a
"magic" slot which contains implementation details.

Objects are normally contained within a lazy evaluation wrapper, made
of an IORef pointing to either code (to evaluate) or an object
(already evaluated).

> newtype Lazy = Lazy
>   { lazyRef :: IORef (Either Code Object) }
>   deriving Eq
>
> instance Show Lazy where
>   show _ = "<lazy>"
>
> data Object = Object
>   { _typeSym :: Symbol
>   , _slots :: Array Int Lazy
>   , _magic :: MagicSlot }
>   deriving (Eq, Show)

The magic slot may contain an arbitrary-precision integer, a symbol,
or some code.

> data MagicSlot
>   = MagicNone
>   | MagicInt { _magicInt' :: Integer }
>   | MagicSymbol { _magicSymbol' :: Symbol }
>   | MagicCode { _magicCode' :: Code }
>   deriving (Eq, Show)

A code object is a tree of expressions. Local bindings are referenced
with de Bruijn indexing (1-based) and global bindings by symbol -
global bindings are always dynamic.

Builtin functions are made of an opcode and a list of code objects
containing arguments.

> data Code
>   = Literal Lazy
>   | Local Int
>   | Global Symbol
>   | Abstract Code
>   | Apply Code Code
>   | Builtin Int [Code]
>   deriving (Eq, Show)

Uniplate is used in order to transform code during application.

> instance Uniplate Code where
>   uniplate (Literal x) = plate Literal |- x
>   uniplate (Local i) = plate Local |- i
>   uniplate (Global s) = plate Global |- s
>   uniplate (Abstract f) = plate Abstract |* f
>   uniplate (Apply f x) = plate Apply |* f |* x
>   uniplate (Builtin n xs) = plate Builtin |- n ||* xs

Create a lazy reference to an object.

> makeLazy :: MonadIO m => Code -> m Lazy
> makeLazy code = Lazy <$> liftIO (newIORef (Left code))
> makeLazy' :: MonadIO m => Object -> m Lazy
> makeLazy' obj = Lazy <$> liftIO (newIORef (Right obj))

Lenses.

> makeLenses ''Object
> slot :: Int -> Traversal' Object Lazy
> slot = (slots .) . ix
>
> makeLenses ''MagicSlot
> magicInt :: Traversal' Object Integer
> magicInt = magic . magicInt'
> magicSymbol :: Traversal' Object Symbol
> magicSymbol = magic . magicSymbol'
> magicCode :: Traversal' Object Code
> magicCode = magic . magicCode'
