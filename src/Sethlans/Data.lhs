> module Sethlans.Data
>   (Symbol(..)
>   ,Lazy(..), Object(..), typeSym, slots, slot, magic
>   ,MagicSlot(..), magicInt, magicSymbol, magicCode
>   ,Code(..), makeLazy, makeLazy'
>   ,apply
>   ,SymbolMap, symbolFromName, symbolToName
>   ,defaultSymbols, defSym
>   ,nullObj, consObj, intObj, symbolObj, codeObj
>   ,nullObj', consObj', intObj', symbolObj', codeObj')
> where
>
> import Sethlans.Data.Apply
> import Sethlans.Data.Construct
> import Sethlans.Data.Symbols
> import Sethlans.Data.Types
