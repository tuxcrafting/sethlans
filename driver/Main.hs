module Main where

import Control.Comonad.Cofree
import Control.Monad
import Control.Monad.IO.Class
import Data.Foldable
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Sethlans.Interpreter

parseMany :: String -> T.Text -> IO [Ast]
parseMany name text = do
  let p = parseSethlans name text
  p' <- either (fail . show) pure p
  let _ :< (AstList asts) = p'
  pure asts

parseFile :: String -> IO [Ast]
parseFile name = parseMany name =<< TIO.readFile name

main :: IO ()
main = do
  either fail pure <=< runInterpreter $ do
    initEnv
    traverse_ (evalAst >=> getLazy) =<<
      liftIO (parseFile "lib/boot.sethlans")
