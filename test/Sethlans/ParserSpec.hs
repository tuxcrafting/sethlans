module Sethlans.ParserSpec (spec) where

import Data.Either
import Test.Hspec
import Sethlans.Parser

spec :: Spec
spec = describe "parser" $ do
  let p = parseSethlans ""
  it "parses empty" $ p "" `shouldSatisfy` isRight
  it "parses no code" $ p "hey\nhow are you" `shouldSatisfy` isRight
  it "parses code" $ p ">1" `shouldSatisfy` isRight
  it "parses complex code" $ p ">(1 3 ''aa (-33 hello-1)() +3b)"
    `shouldSatisfy` isRight
  it "parses interleaved" $ p
    "testy test\n\
    \ >1 (2) 3\n\
    \ > 4 (\n\
    \ blah blah\n\
    \ >)\
    \ blah" `shouldSatisfy` isRight
  it "doesn't parse non-matching parentheses (left)" $ p
    ">((()((()))" `shouldNotSatisfy` isRight
  it "doesn't parse non-matching parentheses (right)" $ p
    ">((()((()))))))" `shouldNotSatisfy` isRight
  it "parses valid escapes" $ p
    ">\"\\n\\v\\w003333\\\\\\\"\"" `shouldSatisfy` isRight
  it "doesn't parse invalid escapes (char)" $ p
    ">\"\\i\"" `shouldNotSatisfy` isRight
  it "doesn't parse invalid escapes (number)" $ p
    ">\"\\u8z\"" `shouldNotSatisfy` isRight
