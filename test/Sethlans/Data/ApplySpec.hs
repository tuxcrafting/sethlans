module Sethlans.Data.ApplySpec (spec) where

import Test.Hspec
import Sethlans.Data

spec :: Spec
spec = describe "function application" $ do
  obj <- runIO $ makeLazy' nullObj
  let try a b = apply a obj `shouldBe` b
      ab = Abstract
      ap = Apply
      l = Local
      obj' = Literal obj
  specify "(\\x.x)0 -> 0" $ try (l 1) obj'
  specify "(\\x.\\y.xy)0 -> \\y.0y" $ try
    (ab (ap (l 2) (l 1)))
    (ab (ap obj' (l 1)))
  specify "(\\x.(\\y.\\z.yx(xz))x)0 -> (\\y.\\z.y0(0z))0" $ try
    (ap (ab (ab (ap (ap (l 2) (l 3)) (ap (l 3) (l 1))))) (l 1))
    (ap (ab (ab (ap (ap (l 2) obj') (ap obj' (l 1))))) obj')
