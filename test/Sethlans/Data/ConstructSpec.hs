module Sethlans.Data.ConstructSpec (spec) where

import Test.Hspec
import Sethlans.Data

spec :: Spec
spec = describe "object construction and deconstruction" $ do
  obj1 <- runIO $ makeLazy' nullObj
  obj2 <- runIO $ makeLazy' nullObj
  let k = consObj obj1 obj2
      i = intObj 3
      s = symbolObj (Symbol 3)
  specify "null is null" $ nullObj `shouldSatisfy` nullObj'
  specify "cons is not null" $ k `shouldNotSatisfy` nullObj'
  specify "cons can be unconsed" $ consObj' k `shouldBe` Just (obj1, obj2)
  specify "null can't be unconsed" $ consObj' nullObj `shouldBe` Nothing
  specify "int can be uninted" $ intObj' i `shouldBe` Just 3
  specify "int can't be unsymboled" $ symbolObj' i `shouldBe` Nothing
  specify "symbol can't be uninted" $ intObj' s `shouldBe` Nothing
