# Sethlans

*Sethlans is dead, long live Sethlans! Reworking the language because
it was a `mess`*.

Sethlans (named after the Etruscan god of craftsmanship) is a pure
functional dynamic programming language.
